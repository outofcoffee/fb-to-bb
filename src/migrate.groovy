/**
 * Examples:
 *
 * Source:
 * https://example.fogbugz.com
 * abc@example.com
 * 
 * Destination:
 * https://bitbucket.org/api/1.0/repositories/example/main/issues
 * abc@example.com
 */
import java.util.prefs.Preferences

class Constants {
    static XML_DATE = new java.text.SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'")
}

class Issue {
    String id
    Person creator
    String title
    String description
    Date created
    List<Comment> comments

    public Issue() {
    }

    public Issue(id, title) {
        this.id = id
        this.title = title
    }
}

class Comment {
    String id
    Person author
    String content
}

class Person {
    String id
    String firstName
    String surname
}

class Repo {
    enum RepoType {
        FOGBUGZ('Fogbugz'),
        BITBUCKET('Bitbucket')

        private repoType

        RepoType(repoType) {
            this.repoType = repoType
        }

        @Override
        public String toString() {
            return this.repoType
        }
    }

    RepoType repoType
    String url
    String email
    String password
    transient String token
}

/**
 * Set the given preference.
 * @param key
 * @param value
 */
void setPreference(String key, String value) {
    Preferences prefs = Preferences.userRoot().node("/com/gatehill/fb-to-bb")
    prefs.put(key, value)
}

/**
 * Get the given preference.
 * @param key
 */
String getPreference(String key) {
    Preferences prefs = Preferences.userRoot().node("/com/gatehill/fb-to-bb")
    return prefs.get(key, null)
}

/**
 * Authenticate against the given repo.
 * @param repo
 */
void authenticate(Repo repo) {
    println "Authenticating against repo: " + repo.url

    switch (repo.repoType) {
        case Repo.RepoType.FOGBUGZ:
            def response = new XmlParser().parse(repo.url + "/api.asp?cmd=logon&email=" + repo.email + "&password=" + repo.password)
            repo.token = response.token.text()
            break;

        case Repo.RepoType.BITBUCKET: // TODO
            def response = new XmlParser().parse(repo.url + "/api.asp?cmd=logon&email=" + repo.email + "&password=" + repo.password)
            repo.token = response.token.text()
            break;
    }


    if (repo.token) {
        println "Authentication successful"
        //println "Token: " + repo.token;
    } else {
        throw new RuntimeException("Authentication failed")
    }
}

/**
 * Log off from the given repo.
 * @param repo
 */
void logOff(repo) {
    println "Logging off from repo: " + repo.url

    switch (repo.repoType) {
        case Repo.RepoType.FOGBUGZ:
            def response = new XmlParser().parse(repo.url + "/api.asp?cmd=logoff&token=" + repo.token)
            break;

        case Repo.RepoType.BITBUCKET: // TODO
            def response = new XmlParser().parse(repo.url + "/api.asp?cmd=logoff&token=" + repo.token)
            break;
    }

    println "Logged off"
}

/**
 * Get existing repository settings.
 */
Repo getConfiguredRepo(Repo.RepoType repoType) {
    // check for existing config
    def repoUrl = getPreference("repo.${repoType}.url")
    if (null != repoUrl) {
        def repo = new Repo()

        repo.repoType = repoType
        repo.url = repoUrl
        repo.email = getPreference("repo.${repoType}.email")
        repo.password = getPreference("repo.${repoType}.password")

        return repo
    }

    // new config
    return configureRepo(repoType)
}

/**
 * Configure the repository settings.
 */
Repo configureRepo(Repo.RepoType repoType) {
    println "You need to configure the $repoType repo"
    def repo = new Repo()

    repo.repoType = repoType
    repo.url = System.console().readLine "Enter repo address: "
    repo.email = System.console().readLine "Enter logon email: "
    repo.password = new String(System.console().readPassword("Enter logon password: "))

    // save for next time
    setPreference("repo.${repoType}.url", repo.url)
    setPreference("repo.${repoType}.email", repo.email)
    setPreference("repo.${repoType}.password", repo.password)

    return repo
}

/**
 * Get all issues.
 */
void downloadAllIssues() {
    def repo = getConfiguredRepo(Repo.RepoType.FOGBUGZ)
    println "Source repo address: " + repo.url

    authenticate(repo)

    // download issues
    def issueXml = new URL(repo.url + "/api.asp?cmd=search&q=&cols=ixBug,correspondent,sTicket,sTitle,dtOpened&token=" + repo.token).text

    // attempt to parse before saving
    def response = new XmlParser().parseText(issueXml)
    println response.cases.case.size() + " cases retrieved"

    // persist locally
    def issues = new File('issues.xml')
    issues.createNewFile()
    issues.write(issueXml)

    logOff(repo)
}

/**
 * Get all issues stored locally.
 */
List<Issue> getAllLocalIssues() {
    // read cases from file and convert to issues
    def issues = new ArrayList<Issue>()

    def response = new XmlParser().parse(new File('issues.xml'))
    def cases = response.cases.case
    println cases.size() + " cases loaded"

    // parse issues
    for (currentCase in cases) {
        def issue = new Issue()
        issue.id = currentCase.ixBug.text()
        issue.title = currentCase.sTitle.text()
        issue.created = Constants.XML_DATE.parse(currentCase.dtOpened.text())

        println "Parsed issue: " + issue.id

        issues.add(issue)
    }

    return issues
}

/**
 * Write issues to the remote repository.
 * @param issues
 */
void writeIssuesToDestination(List<Issue> issues) {
    def repo = getConfiguredRepo(Repo.RepoType.BITBUCKET)
    println "Destination repo address: " + repo.url

    authenticate(repo)

    for (Issue issue : issues) {
        writeIssueToRepo(issue, repo)
    }

    logOff(repo)
}

/**
 * Write the issue to the remote repository.
 * @param issue
 */
void writeIssueToRepo(Issue issue, Repo repo) {
    println "Writing issue $issue to repo $repo.repoType"
}

/*
 * Download issues, parse them and print to console.
 */
println "Downloading issues..."
downloadAllIssues()

println "Printing issues..."
def issues = getAllLocalIssues()

//for (Issue issue : issues) {
//	println issue.id + ": " + issue.title
//}

writeIssuesToDestination(issues)

println "Done."
